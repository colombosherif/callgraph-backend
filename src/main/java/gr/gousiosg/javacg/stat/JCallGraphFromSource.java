/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.gousiosg.javacg.stat;

import static com.veronika.utils.ApplicationParameters.APPLICATION_TEMP_DIR;
import com.veronika.utils.UnzipUtility;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.apache.bcel.classfile.ClassParser;

/**
 *
 * @author expeditive
 */
public class JCallGraphFromSource extends JCallGraphBase {

    public static void main(String[] args) throws IOException {

        JCallGraphFromSource j = new JCallGraphFromSource();

        j.execute(Paths.get("/opt/source.zip"), "");
    }

    public void execute(Path file, String packagePattern) {

        packageNameIncludePattern = packagePattern;

        try {

            rootNode = new Node("project");
            rootNode.setType("root");

            UnzipUtility unzip = new UnzipUtility();

            unzip.unzip(file.toAbsolutePath().toString(), APPLICATION_TEMP_DIR);

            String destDir = APPLICATION_TEMP_DIR + File.separator + unzip.getFirstDirectoryName();

            Files.walk(Paths.get(destDir))
                    .filter((f) -> {

                        return f.getFileName().toString().endsWith(".java");

                    })
                    .forEach((f) -> {

                        JavaFile javaFIle = new JavaFile();
                        javaFIle.parse(f);

                    });

            showNodes(rootNode);

        } catch (IOException e) {

            System.err.println("Error while processing jar: " + e.getMessage());

        }
    }

}
