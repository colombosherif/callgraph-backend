/*
 * Copyright (c) 2017, 2018, TKI and/or Villmond. All rights reserved.
 * TKI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package gr.gousiosg.javacg.stat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author expeditive
 */

public final class JsonParser {       
    
    /**
     *
     * @param <T>
     * @param string
     * @param clazz
     * @return
     */
    public static <T> T getJsonFromString(String string, Class clazz) {

        com.google.gson.JsonParser parser = new com.google.gson.JsonParser();

        JsonObject jsonObject = parser.parse(string).getAsJsonObject();

        GsonBuilder gsonBuilder = new GsonBuilder();

        gsonBuilder = gsonBuilder.setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz").setPrettyPrinting();
        
        Gson gson = gsonBuilder.create();

        return (T) gson.fromJson(jsonObject, clazz);

    }

    /**
     *
     * @param object
     * @return
     */
    public static String getEntityAsJSONString(Object object) {

        GsonBuilder gsonBuilder = new GsonBuilder();

        gsonBuilder = gsonBuilder.setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz").serializeNulls().setPrettyPrinting();

        Gson gson = gsonBuilder.create();

        return gson.toJson(object);

    }

    /**
     *
     * @param object
     * @return
     */
    public static String getEntityAsJSONStringNotNull(Object object) {

        GsonBuilder gsonBuilder = new GsonBuilder();

        gsonBuilder = gsonBuilder.setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz").setPrettyPrinting();

        Gson gson = gsonBuilder.create();

        return gson.toJson(object);

    }

    /**
     *
     * @param <T>
     * @param filePath
     * @param clazz
     * @return
     * @throws IOException
     */
    public static <T> T getJsonFromFile(String filePath, Class clazz) throws IOException {

        com.google.gson.JsonParser parser = new com.google.gson.JsonParser();

        FileReader fileReader = new FileReader(filePath);

        JsonReader jsonReader = new JsonReader(fileReader);

        JsonObject o = parser.parse(jsonReader).getAsJsonObject();

        GsonBuilder gsonBuilder = new GsonBuilder();

        gsonBuilder = gsonBuilder.serializeNulls().setPrettyPrinting();

        Gson gson = gsonBuilder.create();

        return (T) gson.fromJson(o, clazz);

    }


}
