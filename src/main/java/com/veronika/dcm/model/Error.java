/*
 * Copyright (c) 2017, 2018, TKI and/or Villmond. All rights reserved.
 * TKI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.veronika.dcm.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author expeditive
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.UndertowCodegen", date = "2018-02-23T21:39:08.249Z")
public class Error {

    private ErrorError error = null;

    /**
     *
     * @param error
     * @return 
     */
    public Error error(ErrorError error) {
        this.error = error;
        return this;
    }

    /**
     *
     * @return
     */
    @JsonProperty("error")
    public ErrorError getError() {
        return error;
    }

    /**
     *
     * @param error
     */
    public void setError(ErrorError error) {
        this.error = error;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Error error = (Error) o;
        return Objects.equals(error, error.error);
    }

    @Override
    public int hashCode() {
        return Objects.hash(error);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Error {\n");

        sb.append("    error: ").append(toIndentedString(error)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
