/*
 * Copyright (c) 2017, 2018, TKI and/or Villmond. All rights reserved.
 * TKI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.veronika.dcm;

import com.veronika.dcm.handler.PathHandlerProvider;
import com.veronika.dcm.model.Server;
import com.veronika.utils.ApplicationParameters;
import com.veronika.utils.JsonParser;
import io.undertow.UndertowLogger;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.ResourceBundle;
import org.jboss.logging.BasicLogger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * ApplicationConfig is the important main class that initialize all important
 * service, components and features like frameworks for entire project.
 *
 * This class could be reused for other projects that require the initialization
 * of this resources.
 *
 * Initialization of Spring framework, i18N internationalization, server
 * properties, etc.
 *
 * @version 1.0
 * @author Rafael Benedettelli
 */
public final class ApplicationConfig {

    /**
     * Server DTO for data server IP and port.
     */
    public static Server server;

    /**
     * Sprint application context. IOC beans injections.
     */
    public static AnnotationConfigApplicationContext context;

    /**
     * i18n for internationalization messages.
     */
    public static ResourceBundle I18N;

    /**
     * Undertow server logger available for all application artifacts and during
     * all application life-cycle.
     */
    public static BasicLogger logger;

    /**
     * The default path setting in environment variable.
     */
    public static final String APP_HOME = System.getenv("CALLGRAPH_HOME");

    /**
     * Force fatal error exit status code.
     */
    public static final int STATUS_ERROR_EXIT = -1;

    /**
     * The configuration directory..
     */
    public static final String CONFIGURATION_DIRECTORY = "conf";

    /**
     * Initialize method. Start up all services, component and resources
     * required by the application for the rest of the life-cycle.
     *
     * @throws IOException if some problem with data access happen during
     * initialization program.
     */
    public static void init() throws IOException {

        if (APP_HOME == null || APP_HOME.equals("")) {

            System.err.println(":::ENVIRONMENT PROBLEM:::::");

            System.err.println("Must set CONGO_HOME environment variable with absolute path of configuration files. ");

            System.err.println("Sample (CONGO_HOME=\"/opt/congo\") in your SO before the deployment");

            System.exit(STATUS_ERROR_EXIT);

        }

        //Initialize Spring
        context = new AnnotationConfigApplicationContext(
                PathHandlerProvider.class);

        context.start();

        String serverConfigPath = APP_HOME + File.separator + CONFIGURATION_DIRECTORY + File.separator + "server.json";

        //Getting server parametters
        server = JsonParser.getJsonFromFile(serverConfigPath, Server.class);

        //Initializate i18N logic for internationalization program
        initI18n();

    }

    /**
     *
     * This method is useful for obtain the instance required from the Spring
     * IOC context based on the class passed by parameter. Uses this static and
     * always access method for the application entire when wants to use some
     * beans, controller, service or repository instances from the Spring
     * container context.
     *
     * Uses this method for all registers Spring components.
     *
     * @param <T> Is any java class to get from the IOC Spring.
     * @param clazz The java class to obtain from the IOC Spring.
     * @return Instance of the class required from the IOC Spring.
     */
    public static <T> T getBean(Class<T> clazz) {

        if (context == null) {

            context = new AnnotationConfigApplicationContext(
                    PathHandlerProvider.class);

        }

        return (T) context.getBean(clazz);
    }

    /**
     * Method only focused in initialize I18n internationalization framework.
     * This method take internally the properties parameters for country and
     * languages indicated in config.properties file application
     */
    private static void initI18n() throws MalformedURLException {

        Locale currentLocale = new Locale(ApplicationParameters.APPLICATION_LANGUAGE,
                ApplicationParameters.APPLICATION_COUNTRY);

        URL[] urls = {Paths.get(APP_HOME + File.separator + CONFIGURATION_DIRECTORY + File.separator + "i18n").toFile().toURI().toURL()};

        ClassLoader loader = new URLClassLoader(urls);

        I18N = ResourceBundle.getBundle("MessagesBundle", currentLocale, loader);

    }
           

    /**
     * Get main logger for trace info, debug and error information. Undertow
     * server logger available for all application artifacts and during all
     * application life-cycle.
     *
     *
     * @return the Undertow Server Logger. This logger its unique for all
     * application.
     */
    public static BasicLogger getLogger() {                

        return UndertowLogger.REQUEST_LOGGER;

    }

    /**
     * Setter for Undertow Server Logger.
     *
     * @param logger The Undertow unique Logger.
     */
    public static void setLogger(BasicLogger logger) {

        ApplicationConfig.logger = logger;

    }

}
