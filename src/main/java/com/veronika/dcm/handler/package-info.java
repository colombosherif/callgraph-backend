/*
 * Copyright (c) 2017, 2018, TKI and/or Villmond. All rights reserved.
 * TKI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
/**
 * Package for handler controller classes
 * This classes are designed for attend the client request
 * in the MVC model.
 *
 * @author Rafael Benedettelli
 */
package com.veronika.dcm.handler;
