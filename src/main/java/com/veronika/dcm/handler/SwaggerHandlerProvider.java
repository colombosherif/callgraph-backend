/*
 * Copyright (c) 2017, 2018, TKI and/or Villmond. All rights reserved.
 * TKI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.veronika.dcm.handler;

import static com.veronika.dcm.ApplicationConfig.CONFIGURATION_DIRECTORY;
import io.undertow.Handlers;
import io.undertow.server.handlers.PathHandler;
import io.undertow.server.handlers.resource.PathResourceManager;
import io.undertow.server.handlers.resource.ResourceHandler;
import java.io.File;
import java.nio.file.Paths;
import org.springframework.stereotype.Controller;
import static com.veronika.dcm.ApplicationConfig.APP_HOME;
import static com.veronika.utils.ApplicationParameters.HOME_UI;

/**
 *
 * @author expeditive
 */
@Controller
public class SwaggerHandlerProvider extends BaseHandler {

    /**
     *
     * @return
     */
    public PathHandler addHandler() {
                                                                                                            
        final String swaggerUINamedResource = APP_HOME + File.separator
                + CONFIGURATION_DIRECTORY + File.separator + "callgraphSite";

        final String welcameFile = "index.html";

        return Handlers.path()
                // Serve all static files from a folder
                .addPrefixPath(HOME_UI, new ResourceHandler(
                        new PathResourceManager(
                                Paths.get(swaggerUINamedResource)
                        )
                ).addWelcomeFiles(welcameFile));

    }

}
