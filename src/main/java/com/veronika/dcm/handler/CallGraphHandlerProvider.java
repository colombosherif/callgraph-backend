/*
 * Copyright (c) 2017, 2018, TKI and/or Villmond. All rights reserved.
 * TKI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.veronika.dcm.handler;

import com.veronika.dcm.ApplicationConfig;
import static com.veronika.dcm.handler.BaseHandler.logger;
import com.veronika.utils.UndertowHandleRequest;
import gr.gousiosg.javacg.stat.JCallGraph;
import gr.gousiosg.javacg.stat.JCallGraphFromSource;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.RoutingHandler;
import io.undertow.server.handlers.form.FormData;
import io.undertow.server.handlers.form.FormDataParser;
import io.undertow.util.HeaderValues;
import io.undertow.util.Headers;
import io.undertow.util.Methods;
import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpStatus;
import org.springframework.stereotype.Controller;

/**
 *
 * @author expeditive
 */
@Controller
public class CallGraphHandlerProvider extends BaseHandler {

    /**
     *
     */
    public final static String MULTIPART_PACKAGE_PATTERN = "packagePattern";

    /**
     *
     * @param routingHandler
     */
    public void addHandler(RoutingHandler routingHandler) {

        routingHandler.add(Methods.POST, BASE_PATH + "/staticgraph", (HttpServerExchange exchange) -> {

            logger.debug("Executing Rest Service POST generate static callgraph ");

            HeaderValues requestType = exchange.getRequestHeaders().get(Headers.CONTENT_TYPE);

            String requestContentType = requestType.getFirst();

            if (requestContentType.contains("multipart")) {

                createContentMultipart(exchange);

            }
        }).add(Methods.GET, BASE_PATH + "/staticgraph/git/{gitRepository}", (HttpServerExchange exchange) -> {

            logger.debug("Executing Rest Service POST generate static callgraph from git repository ");

            String gitRepositoryURLZip = UndertowHandleRequest.getPathParameter(exchange, "gitRepository");
            
            File downloadZip = new File("source.jar");
            
            FileUtils.copyURLToFile(new URL(gitRepositoryURLZip), downloadZip);
            
            ApplicationConfig.getLogger().info("ZIP: " + downloadZip.getAbsolutePath());

            JCallGraphFromSource jCallGraph = new JCallGraphFromSource();

            jCallGraph.execute(downloadZip.toPath(), "");

            String jsonResponse = jCallGraph.getJsonResult();

            exchange.getResponseHeaders()
                    .put(Headers.CONTENT_TYPE, "application/json");

            exchange.setStatusCode(HttpStatus.SC_OK);

            logger.debug(exchange.getStatusCode());

            exchange.getResponseSender()
                    .send(jsonResponse);

        });

    }

    /**
     *
     * @param exchange
     * @throws Exception
     */
    public void createContentMultipart(HttpServerExchange exchange) throws Exception {

        exchange.getResponseHeaders()
                .put(Headers.CONTENT_TYPE, "application/json");

        FormDataParser parser = UndertowHandleRequest.getFormDataParser(exchange);

        parser.parse((final HttpServerExchange exchangeMultipart) -> {

            FormData data = exchangeMultipart.getAttachment(FormDataParser.FORM_DATA);

            String packagePattern = data.get(MULTIPART_PACKAGE_PATTERN).getFirst().getValue();

            logger.info("Pacakage Pattern: " + packagePattern);

            FormData.FormValue value = data.get("jarFile").getFirst();

            Path jarFile = value.getPath();

            logger.info("jarFile file: " + jarFile.toAbsolutePath().toString());

            String contentType = value.getHeaders().get("Content-Type").getFirst();

            JCallGraph jCallGraph = new JCallGraph();

            jCallGraph.execute(jarFile, packagePattern);

            String jsonResponse = jCallGraph.getJsonResult();

            exchange.getResponseHeaders()
                    .put(Headers.CONTENT_TYPE, "application/json");

            exchange.setStatusCode(HttpStatus.SC_OK);

            logger.debug(exchange.getStatusCode());

            exchange.getResponseSender()
                    .send(jsonResponse);

        });

    }

}
