/*
 * Copyright (c) 2017, 2018, TKI and/or Villmond. All rights reserved.
 * TKI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.veronika.utils;

/**
 * Application parameters is Helper class to map the java constants with key
 * properties in property file.
 *
 * @author Rafael Benedettelli
 */
public final class ApplicationParameters {

    private ApplicationParameters() {

    }
    
       /**
     *
     */
    public static final String APPLICATION_TEMP_DIR = Messages.getString(
            "conf.dcm.temp");
    

    /**
     *
     */
    public static final String APPLICATION_COUNTRY = Messages.getString(
            "app.conf.country");

    /**
     *
     */
    public static final String APPLICATION_LANGUAGE = Messages.getString(
            "app.conf.langugage");

    /**
     *
     */
    public static final String CONF_DCM_ARTIFACT = Messages.getString(
            "conf.dcm.artifact");

    /**
     *
     */
    public static final String CONF_DCM_SCOPE = Messages.getString(
            "conf.dcm.scope");

    /**
     *
     */
    public static final String CONF_DCM_NAME = Messages.getString(
            "conf.dcm.name");

    /**
     *
     */
    public static final String CONF_DCM_VERSION = Messages.getString(
            "conf.dcm.version");

    /**
     *
     */
    public static final String CONF_DCM_DATABAE_HOST = Messages.getString(
            "conf.dcm.database.host");

    /**
     *
     */
    public static final String CONF_DCM_DATABAE_PORT = Messages.getString(
            "conf.dcm.database.port");

    /**
     *
     */
    public static final String CONF_DCM_DATABAE_DB = Messages.getString(
            "conf.dcm.database.db");

    /**
     *
     */
    public static final String CONF_DCM_DATABAE_USER = Messages.getString(
            "conf.dcm.scopedatabase.user");

    /**
     *
     */
    public static final String CONF_DCM_DATABAE_PASSWORD = Messages.getString(
            "conf.dcm.database.password");

    /**
     *
     */
    public static final String CONF_DCM_FILESYSTEM = Messages.getString(
            "conf.dcm.filesystem");

    /**
     *
     */
    public static final String HOME_UI = Messages.getString(
            "conf.ui");

    /**
     * Property username authenticate REST API.
     */
    public static final String CONF_DCM_AUTH_REST_USERNAME = Messages.
            getString("conf.dcm.authenticate.username");

    /**
     * Property password authenticate REST API.
     */
    public static final String CONF_DCM_AUTH_REST_PASSWORD = Messages.
            getString("conf.dcm.authenticate.password");

    /**
     * Property password authenticate REST API.
     */
    public static final String CONF_DCM_AUTH_TICKET_EXPIRED = Messages.
            getString("conf.dcm.authenticate.ticket.expired");

    /**
     * Property allow origin.
     */
    public static final String CONF_REST_SECURITY_ALLOW_ORIGIN = Messages.
            getString("conf.dcm.security.allow_origin");

    /**
     * Property allow methods.
     */
    public static final String CONF_REST_SECURITY_ALLOW_METHOD = Messages.
            getString("conf.dcm.security.allow_methods");

    /**
     * Property result OK key.
     */
    public static final String CONF_REST_RESPONSE_RESULT_OK_KEY = Messages.
            getString("conf.dcm.response.result_ok.key");

    /**
     * Property result OK message.
     */
    public static final String CONF_REST_RESPONSE_RESULT_OK_MESSAGE = Messages.
            getString("conf.dcm.response.result_ok.message");

    /**
     * Property result error key.
     */
    public static final String CONF_REST_RESPONSE_RESULT_ERROR_KEY = Messages.
            getString("conf.dcm.response.result_error.key");

    /**
     * Property result for not implemented functions.
     */
    public static final String CONF_REST_NOT_IMPLEMENTED
            = Messages.getString(
                    "conf.dcm.response.result_not_implemented.message");

    /**
     * Property for date mask format.
     */
    public static final String CONF_REST_DATE_FORMAT_MASK = Messages.getString(
            "conf.dcm.date.format.mask");

    /**
     * Property for date mask format apply for GsonBuilder.
     */
    public static final String CONF_REST_DATE_MASK_GSON = Messages.getString(
            "conf.dcm.date.format.mask.gsonBuilder");

    /**
     *
     */
    private static String confDcmDatabaseModal;

    /**
     *
     * @return
     */
    public static String getConfDcmDatabaseModal() {
        return confDcmDatabaseModal;
    }

    /**
     *
     * @param conf
     */
    public static void setConfDcmDatabaseModal(final String conf) {
        ApplicationParameters.confDcmDatabaseModal = conf;
    }

}
