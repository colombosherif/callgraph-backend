/*
 * Copyright (c) 2017, 2018, TKI and/or Villmond. All rights reserved.
 * TKI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.veronika.utils;

import static com.veronika.dcm.ApplicationConfig.CONFIGURATION_DIRECTORY;
import static com.veronika.dcm.ApplicationConfig.STATUS_ERROR_EXIT;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.io.File;
import static com.veronika.dcm.ApplicationConfig.APP_HOME;

/**
 * Message manager for get the pair values from property file.
 *
 *
 * @author Rafael Benedettelli
 */
public final class Messages {

    /**
     * The resource property file.
     */
    private static final String BUNDLE_NAME = "configuration";

    /**
     * The resource Bundle object for manage property file.
     */
    public static ResourceBundle RESOURCE_BUNDLE;

    static {

        try {

            URL[] urls = {Paths.get(APP_HOME + File.separator + CONFIGURATION_DIRECTORY + File.separator).toFile().toURI().toURL()};

            ClassLoader loader = new URLClassLoader(urls);

            RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME, Locale.getDefault(), loader);

        } catch (IOException e) {

            System.err.println("Fatal error at try to get configuration file");
            System.err.print(e);
            System.exit(STATUS_ERROR_EXIT);

        }

    }

    /**
     * Default constructor.
     */
    private Messages() {

    }

    /**
     * This method return the value from a valid key in the respective property
     * file resource.
     *
     * @param key the key in the property file
     *
     * @return String represents the value of the key in the property file
     */
    public static String getString(String key) {
        try {
            return RESOURCE_BUNDLE.getString(key);
        } catch (MissingResourceException e) {
            return '!' + key + '!';
        }
    }
}
