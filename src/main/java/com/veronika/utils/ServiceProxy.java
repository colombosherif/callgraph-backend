/*
 * Copyright (c) 2017, 2018, TKI and/or Villmond. All rights reserved.
 * TKI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.veronika.utils;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

/**
 * Service Proxy its for general use and make HTTP connections via proxy server
 * with or not authentications.
 *
 * @author Rafael Benedettelli - TKI
 */
public final class ServiceProxy {

    /**
     * The proxy HTTP URL.
     */
    private String proxyHttp;

    /**
     * The proxy username to authenticate.
     */
    private String proxyUser;

    /**
     * The proxy user password.
     */
    private String proxyPassword;

    /**
     * The proxy port.
     */
    private String proxyPort;

    /**
     * This establish a connection with proxy server via HTTP protocol.
     */
    public void connect() {

        if (!proxyHttp.trim().equals("")) {

            Authenticator.setDefault(
                    new Authenticator() {

                /**
                 * {@inheritDoc}
                 */
                @Override
                public PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(
                            proxyUser, proxyPassword.
                                    toCharArray());
                }
            }
            );

            System.setProperty("http.proxyHost", proxyHttp);

            System.setProperty("http.proxyPort", proxyPort);

        }

    }

    /**
     * Getter Proxy HTTP.
     *
     * @return the URL Proxy.
     */
    public String getProxyHttp() {
        return proxyHttp;
    }

    /**
     * Setter for proxy HTTP.
     *
     * @param proxyHttp the URL proxy.
     */
    public void setProxyHttp(String proxyHttp) {
        this.proxyHttp = proxyHttp;
    }

    /**
     * Getter for username proxy for authenticate.
     *
     * @return the username.
     */
    public String getProxyUser() {
        return proxyUser;
    }

    /**
     * Setter
     *
     * @param proxyUser
     */
    public void setProxyUser(String proxyUser) {
        this.proxyUser = proxyUser;
    }

    /**
     * Getter for proxy password.
     *
     * @return the proxy password.
     */
    public String getProxyPassword() {
        return proxyPassword;
    }

    /**
     * Setter for proxy password.
     *
     * @param proxyPassword the password for user proxy.
     */
    public void setProxyPassword(String proxyPassword) {
        this.proxyPassword = proxyPassword;
    }

    /**
     * Getter for proxy server port.
     *
     * @return the proxy server port number.
     */
    public String getProxyPort() {
        return proxyPort;
    }

    /**
     * Setter for proxy server port.
     *
     * @param proxyPort the proxy server port number.
     */
    public void setProxyPort(String proxyPort) {
        this.proxyPort = proxyPort;
    }

}
