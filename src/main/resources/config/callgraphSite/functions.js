
var file = null;

var validExtensions = ["jar", "JAR", "war","WAR","ear","EAR"];

var errorMessages = document.getElementById('errorMessage');
errorMessages.style.display='none';

var header = document.getElementById('header');
header.style.display='none';


  /**
   *
   *  @method
   *  Method for detect the drop action event made 
   *  for user in zone of load file. 
   *  This method has the logic for execute properly actions
   *  nedeed  
   *  @param {ev} event - The event object with event information.
   *
   */
 function dropHandler(ev) {

  errorMessages.style.display='none';
  header.style.display='none';
  
  console.log("Drop");

  ev.preventDefault();
  
  // If dropped items aren't files, reject them  
  var dt = ev.dataTransfer;

  var formData = new FormData();

  if (dt.items) {

  //Chrome and newest explorers.

    // Use DataTransferItemList interface to access the file(s)
    for (var i=0; i < dt.items.length; i++) {

      if (dt.items[i].kind == "file") {

        file = dt.items[i].getAsFile();

        console.log("... file[" + i + "].name = " + file.name);
        console.log("... file[" + i + "].path = " + file.path);
        console.log("... file[" + i + "].path = " + file.size);

        document.getElementById("file").innerHTML = file.name;
      
                       
      }
    }

  } else {
    
    // Use DataTransfer interface to access the file(s)
    for (var i=0; i < dt.files.length; i++) {

      //Firefox 9

      console.log("... file[" + i + "].name = " + dt.files[i].name);
      console.log("... file[" + i + "].path = " + dt.files[i].path);
      console.log("... file[" + i + "].size = " + dt.files[i].size);

      document.getElementById("file").innerHTML = dt.files[i].name;


      file = dt.files[i];

      
    }
  }


   var newDoc = {};  
   newDoc.idPersona = '';
   newDoc.name='';
   newDoc.objectId='';

   if (this.validateFile(file)){

       //Validations
       var fileName = file.name;  

       if (fileName.length > 40){

          fileName = fileName.substring(0,40) + " ...";
          
       }


       document.getElementById("controls").style.display='block';

  }else{

    return false;

  }

   return dt;
}

 
  /**
   *
   *  @method
   *  Method for detect the drag over action event made 
   *  for user in zone of load file. 
   *  This method has the logic for execute properly actions
   *  nedeed.
   *  @param {ev} event - The event object with event information.
   *
   */
 function dragoverHandler(ev) {

  errorMessages.style.display='none';
  header.style.display='none';
  console.log("dragOver");


  var imgDrag = document.getElementById('img_drag');
  imgDrag.style.opacity=1;

    var titleDrag = document.getElementById('title_drag');
    titleDrag.style.color='#616161';
  

  // Prevent default select and drag behavior
  ev.preventDefault();

 }

 /**
   *
   *  @method
   *  Method for detect the drag end action event made 
   *  for user in zone of load file. 
   *  This method has the logic for execute properly actions
   *  nedeed.
   *  @param {ev} event - The event object with event information.
   *
   */
 function dragendHandler(ev) {
 
  console.log("dragEnd");



  // Remove all of the drag data
  var dt = ev.dataTransfer;
   
    if (dt.items) {
      // Use DataTransferItemList interface to remove the drag data
      for (var i = 0; i < dt.items.length; i++) {
        dt.items.remove(i);
      }
    } else {
      // Use DataTransfer interface to remove the drag data
      ev.dataTransfer.clearData();
    }

    return dt;

  }


 function dragLeave(ev){

    console.log("drag leave");

    var imgDrag = document.getElementById('img_drag');
  
    imgDrag.style.opacity=0.5;

    var titleDrag = document.getElementById('title_drag');
    titleDrag.style.color='#cecece';
  


  }


 /**
  * This method validate an upload file 
  * from user. The validate apply rules for sizing and mime type.   
  * The method provide sefl the alerts message for errors.
  * @param {String} dateHumanRedable the date in human redeable format (20/07/2017)
  * @return {Boolean} true if valid and false in other wise.
  */
  function validateFile(file){

   var fileName = file.name;

   var parts = fileName.split(".");

   var ext = parts[1];

   var validate = validExtensions.some(function(extValid) {
      
      return extValid == ext;

    });


    if (!validate){

        alert("El archivo que subio tiene una entension invalida");

        document.location.reload();

        return false;
    }

    var sizeInMB = (file.size / (1024*1024)).toFixed(2);

    var MAX_PERM_SIZE = 10024;

    if (sizeInMB > MAX_PERM_SIZE){        
 
      
         alert("Supero el tamaño maximo permitido " + MAX_PERM_SIZE + " Megas ");


      return false; 

    }

    return true;
   

  }

  function changeInputGit(){

    var gitRepoUrl = document.getElementById('gitRepository').value;

    var btn = document.getElementById('btnAnalizeFromGit');

    if (validateGitUrl(gitRepoUrl)){ 

       btn.style.display='block'; 

       errorMessages.style.display='none';

    }else{ 
  
      errorMessages.style.display='block';
  
      errorMessages.innerHTML = 'La url indicada no es de un proyecto git valido';

      btn.style.display='none'; 

    }
  }

  function validateGitUrl(url){

      if (url ==null){

        return false;
      }

      if (!url.endsWith(".git")){

        return false;
      }

      if (!url.startsWith("http")){

        return false;
      }

      return true;

  }


  function analizeFromGitRepository(){


      var btn = document.getElementById('btnAnalizeFromGit');
      btn.disabled=true;

      //preloader
      var imgDrag = document.getElementById('img_drag');
      imgDrag.src='loader.gif';


      var gitRepoUrl = document.getElementById('gitRepository').value;

      gitRepoUrl = gitRepoUrl.substring(0,gitRepoUrl.length-4);

      gitRepoUrl += "/archive/master.zip";

      let xhr = new XMLHttpRequest();
      xhr.open("GET", 'http://190.194.237.220:8080/api/public/callgraph/v1/staticgraph/git/?gitRepository=' + encodeURIComponent(gitRepoUrl));
      xhr.send();


      xhr.onreadystatechange = function() {

        if (this.readyState == 4 && this.status == 200) {

            imgDrag.src='jar.png';
            btn.disabled=false;
            drawCallgraph(this.responseText);
       
        }
     
     };

      
  }


  function analizeJar(){

    var btn = document.getElementById('btnAnalizeJar');
      btn.disabled=true;

    var imgDrag = document.getElementById('img_drag');
    imgDrag.src='loader.gif';


    errorMessages.style.display='none';
    header.style.display='none';

    var packagePattern = document.getElementById("packagePattern").value;

    let formData = new FormData();
    formData.append("jarFile", file);        
    formData.append("packagePattern", packagePattern); 

    let xhr = new XMLHttpRequest();
    xhr.open("POST", 'http://190.194.237.220:8080/api/public/callgraph/v1/staticgraph');
    xhr.send(formData);


    xhr.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {

          imgDrag.src='jar.png';
          btn.disabled=false;

          drawCallgraph(this.responseText);
     
      }
   };


  }


//-----------------------------------------------------------
//Callgraph

var margin = {top: 20, right: 120, bottom: 20, left: 120},
    width = 960 - margin.right - margin.left,
    height = 800 - margin.top - margin.bottom;

var i = 0,
    duration = 750,
    root;

var tree = d3.layout.tree()
    .size([height, width]);

var diagonal = d3.svg.diagonal()
    .projection(function(d) { return [d.y, d.x]; });

var svg = d3.select("body").append("svg")
    .attr("width", "100%")
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


function drawCallgraph(jsonResonse){

  root = JSON.parse(jsonResonse);

  root.x0 = height / 2;
  root.y0 = 0;

  function collapse(d) {
    if (d.children) {
      d._children = d.children;
      d._children.forEach(collapse);
      d.children = null;
    }
  }

  if (root.children == null){

      var packagePattern = document.getElementById("packagePattern").value;

      var erroMsg = "No se encontraron clases ";

      if (packagePattern != ''){

        erroMsg += " para el filtro de paquete " + packagePattern;


      }

       errorMessages.style.display='block';
       header.style.display='none';

       errorMessages.innerHTML = erroMsg;

       return;


  }


  header.style.display='block';

  document.getElementById('container').style.display='none';

  root.children.forEach(collapse);
  update(root);

}

d3.select(self.frameElement).style("height", "800px");

function update(source) {

  // Compute the new tree layout.
  var nodes = tree.nodes(root).reverse(),
      links = tree.links(nodes);

  // Normalize for fixed-depth.
  nodes.forEach(function(d) { d.y = d.depth * 180; });

  // Update the nodes…
  var node = svg.selectAll("g.node")
      .data(nodes, function(d) { return d.id || (d.id = ++i); });

  // Enter any new nodes at the parent's previous position.
  var nodeEnter = node.enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
      .on("click", click);

  nodeEnter.append("circle")
      .attr("r", 1e-6)
      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  nodeEnter.append("text")
      .attr("x", function(d) { return d.children || d._children ? -10 : 10; })
      .attr("dy", ".35em")
      .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
      .text(function(d) { return d.name; })
      .style("fill-opacity", 1e-6);

  // Transition nodes to their new position.
  var nodeUpdate = node.transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

  nodeUpdate.select("circle")
      .attr("r", 4.5)
      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  nodeUpdate.select("text")
      .style("fill-opacity", 1);

  // Transition exiting nodes to the parent's new position.
  var nodeExit = node.exit().transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
      .remove();

  nodeExit.select("circle")
      .attr("r", 1e-6);

  nodeExit.select("text")
      .style("fill-opacity", 1e-6);

  // Update the links…
  var link = svg.selectAll("path.link")
      .data(links, function(d) { return d.target.id; });

  // Enter any new links at the parent's previous position.
  link.enter().insert("path", "g")
      .attr("class", "link")
      .attr("d", function(d) {
        var o = {x: source.x0, y: source.y0};
        return diagonal({source: o, target: o});
      });

  // Transition links to their new position.
  link.transition()
      .duration(duration)
      .attr("d", diagonal);

  // Transition exiting nodes to the parent's new position.
  link.exit().transition()
      .duration(duration)
      .attr("d", function(d) {
        var o = {x: source.x, y: source.y};
        return diagonal({source: o, target: o});
      })
      .remove();

  // Stash the old positions for transition.
  nodes.forEach(function(d) {
    d.x0 = d.x;
    d.y0 = d.y;
  });
}

// Toggle children on click.
function click(d) {
  if (d.children) {
    d._children = d.children;
    d.children = null;
  } else {
    d.children = d._children;
    d._children = null;
  }
  update(d);
}


