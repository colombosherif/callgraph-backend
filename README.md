# ECM #

Este proyecto implementa un gestor documental liviano diseñado para el manejo de grandes volúmenes de documentos. 

# Instrucciones de construccion #
Configurar la variable de entorno VILLMOND_ECM_HOME apuntando un path valido en su equipo (ej: VILLMOND_ECM_HOME=/opt/congo)
Debe tener instalado Mongo db en su equipo.
Opcionalmente puede definir la variable de entorno MONGO_DB apuntando a una instalacion de mongo externa (ej: VILLMOND_ECM__DB="mongodb://172.17.0.2:27017/dcmDB")


## Arquitectura ##
El proyecto está basado en MongoDB y Spring framework. El proyecto es una aplicación autónoma gestionada por Spring Boot.

## Autores ##

Ing. Rafael Benedettelli
Ing. Victorio Bentivogli


